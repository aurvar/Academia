var scope ={
    container : document.querySelector('[data-module="app"]'),
    init(){
        this.container.innerHTML=this.template;
        scope.render();
    },
    render(){
        this.mount();
    },
    mount(){
        document.querySelector('[data-value="seconds"]').innerHTML=this.data.seconds;
        //document.querySelector('[data-value="minutes"]').innerHTML=this.data.minutes;
        var list =document.querySelectorAll('[data-value]');
        for (i=0; i<list.length;i++){
            if(list.item(i).getAttribute("class")=="minutes"){
                list.item(i).value=this.data.minutes;
            }
        }
        Object.keys(scope.data).forEach(function(key) {
            //console.log(key, scope.data[key]);
            for (i=0; i<list.length;i++){
                if(list.item(i).getAttribute("class")==key){
                    list.item(i).innerHTML=scope.data[key];
                    console.log(list.item(i).getAttribute("class"),key, scope.data[key]);
                }
            }
        });

        //document.querySelectorAll('[data-value');
    },
    template:`
    <article class="reloj">
    <section class="display">   
        <span class="minutes" data-value="minutes"></span>   
        <span class="seconds" data-value="seconds"></span>        
    </section>
    <section class="controls">  
        <button>Start</button>
        <button>Pause</button>
        <button>Stop</button>
    </section>
    </article>`,
    data:{seconds : 0,minutes:10}
}


scope.init()

setInterval(function(){
    scope.data.seconds++;
    if (scope.data.seconds ==10)
    {
        scope.data.seconds=0
        scope.data.minutes++;
    }
    scope.render();
},1000)


